<?php

namespace Lms3\Lms3videojumper\Service;

use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility as Utility;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Sergey Borulko <borulkosergey@icloud.com>, LEARNTUBE
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

class VideoStorageService implements SingletonInterface
{
    /**
     * @var \TYPO3\CMS\Core\Resource\ResourceFactory
     * @inject
     */
    protected $resourceFactory;

    /**
     * Contains the path to video storage
     *
     * @var \TYPO3\CMS\Core\Resource\Folder
     */
    protected $videoStorage = null;

    /**
     * @var \TYPO3\CMS\Core\Resource\ResourceStorage
     */
    protected $defaultStorage = null;


    /**
     * @param string $tsDefinedVideoStoragePath
     * @throws \Exception
     */
    public function __construct($tsDefinedVideoStoragePath)
    {
        $this->resourceFactory = Utility::makeInstance(ResourceFactory::class);
        if (empty($this->resourceFactory)) {
            throw new \Exception('Resource Factory is not initialized');
        }

        $this->defaultStorage = $this->resourceFactory->getDefaultStorage();
        if (empty($this->defaultStorage)) {
            throw new \Exception('Default storage is not initialized');
        }

        if (empty($tsDefinedVideoStoragePath)) {
            throw new \Exception('Video storage path is not specified. Check TS constants.');
        }

        $this->videoStorage = $this->defaultStorage->getFolder($tsDefinedVideoStoragePath);
    }


    /**
     * Contains all the existing record storages
     *
     * @api
     * @return array
     */
    public function getUserStorageList()
    {
        $users = [];
        foreach ($this->defaultStorage->getFoldersInFolder($this->videoStorage) as $folder) {
            $users[] = $folder->getName();
        }

        return $users;
    }

    /**
     * Returns storage only for current user
     *
     * @api
     * @return string
     */
    public function getCurrentUserStorage()
    {
        $currentUserStorage = $this->getBackEndUserName();
        if (empty($currentUserStorage)) {
            return '';
        }

        if (!in_array($currentUserStorage, $this->getUserStorageList())) {
            return '';
        }

        return $currentUserStorage;
    }

    /**
     * Returns all the date folder names inside passed storage
     *
     * @api
     * @param string $storage Contains the storage title
     * @param bool $includeContent If TRUE, the content inside certain date folder also would be returned
     * @returns array
     */
    public function getDatesByStorage($storage, $includeContent = false)
    {
        $userFolder = $this->defaultStorage->getFolder($this->videoStorage->getName() . '/' . $storage);

        $result = [];
        foreach ($this->defaultStorage->getFoldersInFolder($userFolder) as $key => $folder) {
            $result[$key]['title'] = $folder->getName();

            if ($includeContent) {
                $result[$key]['files'] = $this->getVideosInDateFolder($folder);
            }
        }
        return $result;
    }
    /**
     * Returns the current BE user name
     *
     * @api
     * @return string
     */
    public function getBackEndUserName()
    {
        if (empty($GLOBALS['BE_USER'])) {
            return '';
        }

        return $GLOBALS['BE_USER']->user['username'];
    }

    /**
     * @param \TYPO3\CMS\Core\Resource\File[] $dateFolder
     * @return array
     */
    protected function getVideosInDateFolder($dateFolder)
    {
        $videos = [];
        foreach ($this->defaultStorage->getFilesInFolder($dateFolder) as $file) {
            $videos[] = $file->getName();
        }

        return $videos;
    }

    /**
     * @return \TYPO3\CMS\Core\Resource\Folder
     */
    public function getVideoStorage()
    {
        return $this->videoStorage;
    }

    /**
     * Returns the default path like [fileadmin/records/]
     *
     * @return string
     */
    public function getBasePath()
    {
        $defaultStorageName = $this->videoStorage->getStorage()->getConfiguration()['basePath'];

        return $defaultStorageName . ltrim($this->videoStorage->getIdentifier());
    }

    /**
     * Returns video file by passed path
     *
     * @param $path
     * @return \TYPO3\CMS\Core\Resource\FileInterface
     */
    protected function getVideoFile($path)
    {
        return $this->videoStorage->getStorage()->getFile($path);
    }

    /**
     * @param $path
     * @param $name
     * @api
     */
    public function renameVideoFile($path, $name)
    {
        $file = $this->getVideoFile($path);
        if (empty($file)) {
            return;
        }

        $file->rename($name .  '.' . $file->getProperty('extension'));
    }

    /**
     * Deletes video file by path
     *
     * @param $path
     * @api
     */
    public function removeVideoFile($path)
    {
        $file = $this->getVideoFile($path);
        if (empty($file)) {
            return;
        }

        $file->delete();
    }
}
