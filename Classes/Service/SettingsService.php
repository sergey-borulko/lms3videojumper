<?php
namespace Lms3\Lms3videojumper\Service;

use TYPO3\CMS\Core\SingletonInterface;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Sergey Borulko <borulkosergey@icloud.com>, LEARNTUBE
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

class SettingsService implements SingletonInterface
{
    /**
     * CONFIGURATION_TYPE_SETTINGS
     * @var array
     */
    protected $settings;


    /**
     * @param \TYPO3\CMS\Extbase\Configuration\ConfigurationManager $configurationManager
     * @return void
     */
    public function injectConfigurationManager(\TYPO3\CMS\Extbase\Configuration\ConfigurationManager $configurationManager) {
        $this->settings = $configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManager::CONFIGURATION_TYPE_SETTINGS);
        if (empty($this->settings)) {
            throw new \Exception('No configuration found inside Settings Service');
        }
    }

    /**
     * If 'settingsIdentifier' exists inside plugin settings, - return it
     *
     * @api
     * @param $settingsIdentifier string
     * @return array | NULL
     */
    public function getSettings($settingsIdentifier)
    {
        return $this->settings[$settingsIdentifier];
    }

}
