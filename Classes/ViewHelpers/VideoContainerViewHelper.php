<?php

namespace Lms3\Lms3videojumper\ViewHelpers;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Sergey Borulko <borulkosergey@icloud.com>, LEARNTUBE
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;

class VideoContainerViewHelper extends AbstractTagBasedViewHelper
{
    /**
    * @var string
    */
    protected $tagName = 'video';

    /**
    * @var \TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder
    * @inject
    */
    protected $urlBuilder = null;


    /**
    * @param array $settings
    * @return string
    */
    public function render($settings = [])
    {
        $this->bindAttributes($settings);
        $this->tag->setContent($this->renderChildren());

        if (strpos($this->renderChildren(), '.mp3') !== false) {
            $this->tag->addAttribute('class', $this->tag->getAttribute('class') . ' ' . 'audio');
        }

        return $this->tag->render();
    }

    /**
     * Sets attributes based on settings in the FlexForm
     *
     * @param array $settings
     * @return void
    */
    protected function bindAttributes($settings)
    {
        $this->tag->addAttribute('class', 'video-js vjs-default-skin vjs-big-play-centered');
        $this->tag->addAttribute('preload', 'auto');
        $this->tag->addAttribute('controls', '');

        $isAutoplay = (bool)$settings['autostart'];
        $isExternal = (bool)$settings['isExternal'];

        if ($isAutoplay) {
            $this->tag->addAttribute('autoplay', '');
        }

        $poster = $settings['posterImage'];
        if ($poster && $isExternal === false) {
            $this->tag->addAttribute('poster', $poster);
        }

        $externalUrl = $settings['externalUrl'];
        if ($externalUrl && $isExternal) {
            $this->tag->addAttribute('data-setup', $this->getExternalSetup($externalUrl, $isAutoplay));
        }

        $this->tag->addAttribute('data-skipping', $settings['allowSkip']);

        $redirectPageId = intval($settings['nextPage']);
        if (empty($redirectPageId)) {
            return;
        }
        $uri = $this->urlBuilder->setTargetPageUid($redirectPageId)->build();

        $this->tag->addAttribute('data-finish', $uri);
    }

    /**
     * Returns video type by it's url
     *
     * @param string $url External video url like (https://www.youtube.com/watch?v=xjS6SftYQaQ)
     * @return string [ youtube | vimeo ]
     */
    protected function getExternalVideoTypeByUrl($url)
    {
        $type = 'youtube';
        if (strpos($url, 'vimeo') !== false) {
            $type = 'vimeo';
        }

        return $type;
    }

    /**
     * Returns data-setup settings
     *
     * @param string $externalUrl
     * @param bool $isAutoplay
     * @return string
     */
    protected function getExternalSetup($externalUrl, $isAutoplay)
    {
        $type = $this->getExternalVideoTypeByUrl($externalUrl);

        $autoplay = '';
        if ($isAutoplay) {
            $autoplay = ', "'. $type .'": { "autoplay": 1 }';
        }

        $settings = '{"techOrder": ["' . $type . '"],' .
        '"sources": [{ "type": "video/' . $type . '", "src": "' . $externalUrl . '"}]' . $autoplay . ' }';

        return $settings;
    }
}
