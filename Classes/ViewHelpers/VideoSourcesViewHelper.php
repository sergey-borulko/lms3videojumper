<?php
namespace Lms3\Lms3videojumper\ViewHelpers;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Sergey Borulko <borulkosergey@icloud.com>, LEARNTUBE
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


class VideoSourcesViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper
{

	/**
	 * @var string
	 */
	protected $tagName = 'source';

	/**
	 * @param string $sources
	 * @return string
	 */
	public function render($sources)
	{
		if (empty($sources)) {
			return '';
		}

		$tags = '';
		foreach(explode(',', $sources) as $source) {
			$this->tag->addAttribute('src', $source);
			$this->tag->addAttribute('type', mime_content_type($source));
			$tags[] = $this->tag->render();
		}

		return implode(' ', $tags);
	}

}