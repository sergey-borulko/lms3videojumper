<?php

namespace Lms3\Lms3videojumper\Controller;

use TYPO3\CMS\Core\Utility\GeneralUtility as Utility;
use Lms3\Lms3videojumper\Service\VideoStorageService as VideoStorage;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Sergey Borulko <borulkosergey@icloud.com>, LEARNTUBE
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

class ModuleController extends ActionController
{
    /**
     * @var \Lms3\Lms3videojumper\Service\VideoStorageService
     */
    protected $storageService = null;


    public function initializeAction()
    {
        parent::initializeAction();

        if (empty($this->storageService)) {
            $this->storageService = Utility::makeInstance(VideoStorage::class, $this->settings['videoStorage']);
        }
    }

    public function showAction()
    {
    }

    public function uploadAction()
    {
        $file = null;
        if ($this->request->hasArgument('upload')) {
            $file = $this->request->getArgument('upload')['screenRecord'];
        }

        if (!is_array($file) || !array_key_exists('tmp_name', $file)) {
            return;
        }

        $userName = $this->storageService->getBackEndUserName();
        $storage = PATH_site . $this->storageService->getBasePath() . $userName . '/' . date('Y-m-d') . '/';

        Utility::mkdir_deep($storage);
        Utility::upload_copy_move($file['tmp_name'], $storage . date('H-i') . '.webm');
    }

    /**
     * @param bool $currentOnly
     * @return void
     */
    public function listAction($currentOnly = false)
    {
        $storageList = [];
        if ($currentOnly) {
            $storageList[] = $this->storageService->getCurrentUserStorage();
        } else {
            $storageList = $this->storageService->getUserStorageList();
        }

        $folders = [];
        foreach ($storageList as $key => $storage) {
            $folders[$key]['title'] = $storage;
            $folders[$key]['dates'] = $this->storageService->getDatesByStorage($storage, true);
        }

        $this->view->assign('allFolders', $folders);
        $this->view->assign('currentUserOnly', $currentOnly);
        $this->view->assign('basePath', '../' . $this->storageService->getBasePath());
    }

    /**
     * Removes video file by passed path
     *
     * @return void
     */
    public function removeVideoAction()
    {
        $path = preg_replace('/^..\/\w+/', '', (string)Utility::_GP('path'));

        $this->storageService->removeVideoFile($path);
    }

    /**
     * Updates name for given video file
     *
     * @return void
     */
    public function renameVideoAction()
    {
        $path = preg_replace('/^..\/\w+/', '', (string)Utility::_GP('path'));

        $newName = Utility::_GP('name');
        if (empty($newName)) {
            return;
        }

        $this->storageService->renameVideoFile($path, $newName);
    }
}
