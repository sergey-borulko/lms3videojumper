module.exports = function(grunt) {

	grunt.initConfig({
		uglify: {
			my_target: {
				files: {
					'JavaScript/fe-main.min.js': [
						'JavaScript/Modules/**/*.js'
					],
					'JavaScript/jumper-be-main.min.js': [
						'JavaScript/Modules/BE/**/*.js'
					]
				}
			}
		},
		cssmin: {
			target: {
				files: {
					'StyleSheets/fe-main.min.css': [
						'StyleSheets/main.css'
					]
				}
			}
		},
		compass: {
			dist: {
				options: {
					sassDir: 'StyleSheets/Modules',
					cssDir: 'StyleSheets',
					environment: 'production',
					raw: 'preferred_syntax = :sass\n',
					javascriptsDir: 'JavaScript',
					force: true
				}
			},
			options: {
				require: 'susy'
			}
		},
		jscs: {
			src: ['JavaScript/Modules/**/*.js'],
			options: {
				config: ".jscsrc",
				excludeFiles: [],
				esnext: true,
				verbose: true,
				requireCurlyBraces: [ "if" ]
			}
		},
		jshint: {
			options: {
				curly: true,
				eqeqeq: true,
				eqnull: true,
				browser: true,
				globals: {
					jQuery: true
				},
				ignores: ['Vendor/**/*.js']
			},
			all:  ['Vendor/**/*.js']
		},
		watch: {
			files: [
				'JavaScript/Modules/**/*.js',
				'StyleSheets/Modules/**/*.sass'
			],
			tasks: ['jshint', 'jscs', 'uglify', 'compass']
		}
	});

	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-jscs');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.loadNpmTasks('grunt-sass');

	grunt.registerTask('default', ['watch']);
};
