modules.define('recorder', ['recordUploader', 'browser'], function(provide, Uploader, Browser) {

	var _self,
		_selector = 'recorder',
		_instance = null;

	provide({

		init: function () {
			_self = this;

			if (this.hasExist() === false) {
				return;
			}

			_instance = videojs(_selector, {
				controls: true,
				width: 640,
				height: 480,
				plugins: {
					record: {
						audio: true,
						video: true,
						maxLength: 99999
					}
				}
			});

			this.bindFinishEvent();
			Uploader.init();
		},

		bindFinishEvent: function() {
			_instance.on('finishRecord', function() {
				_instance.recorder.stopDevice();
				Uploader.upload(_self.getRecorderFile());
			});
		},

		/**
		 * Returns the file that user has recorded
		 *
		 * @returns {blob}
		 */
		getRecorderFile: function() {
			var file;
			if (Browser.isChrome()) {
				file = _instance.recordedData.video;
			} else {
				file = _instance.recordedData;
			}

			return file;
		},

		/**
		 * Returns TRUE if recorder exists on the current page
		 * @returns {boolean}
		 */
		hasExist: function() {
			return document.getElementById(_selector) !== null;
		}

	});
});