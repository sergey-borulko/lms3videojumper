modules.define('videoTree', ['preview', 'storageAjax'], function(provide, Preview, StorageAPI) {

	var _self,
		_selector = 'storage-tree',
		_modeTriggerSelector = 'onlyCurrentUser',
		_instance = null;

	provide({

		init: function () {
			_self = this;

			if (this.isFound() === false) {
				return;
			}

			_instance = $('#' + _selector).jstree({
				"types" : {
					"user" : {
						"icon" : "glyphicon glyphicon-user"
					},
					"time" : {
						"icon" : "glyphicon glyphicon-time"
					},
					"video" : {
						"icon" : "glyphicon glyphicon-facetime-video"
					}
				},
				"contextmenu" : {
					"items": function($node) {

						return _self.buildContextMenu($node)
					}
				},
				"plugins" : ["sort", "types", "wholerow", "contextmenu"]
			});


			this.bindNodeSelection();
			this.bindUserMode();
		},

		buildContextMenu: function($node) {
			if ($node.type === 'time') {
				return;
			} else if ($node.type === 'user') {
				return;
			}

			return {
				"Rename": {
					"separator_before": false,
					"separator_after": false,
					"label": "Rename",
					"action": function (data) {
						var inst = $.jstree.reference(data.reference),
							node = inst.get_node(data.reference);
						var currentNodeLink = $('#' + $($node).attr('id')).find('a:last-child');

						var name = prompt("Title", $(currentNodeLink).text());
						if (name == null) {
							return;
						} if (name.indexOf('.webm') != -1) {
							name = name.replace(".webm", "");
						}

						StorageAPI.sendRenameRequest(_self.getPathByNodes(inst.get_path(node)), name);

						$(currentNodeLink).text(name + '.webm');
					}
				},
				"Remove": {
					"separator_before": false,
					"separator_after": false,
					"label": "Remove",
					"action": function (data) {
						var inst = $.jstree.reference(data.reference),
							node = inst.get_node(data.reference);

						StorageAPI.sendRemoveRequest(_self.getPathByNodes(inst.get_path(node)));

						$('#' + $($node).attr('id')).remove();
					}
				}
			};
		},

		/**
		 * Binds action when appropriate video was selected
		 */
		bindNodeSelection: function() {
			$(_instance).on("changed.jstree", function (e, data) {
				var nodes = data.instance.get_path(data.node);

				if (data.node.type === 'time') {
					return;
				} else if (data.node.type === 'user') {
					return;
				}

				Preview.updateVideo(_self.getPathByNodes(nodes));
			});
		},

		/**
		 * Returns the path on a filesystem where video is located
		 * @param nodes
		 * @returns {*}
		 */
		getPathByNodes: function(nodes) {
			for (var i = 0; i < nodes.length; i++) {
				nodes[i] = nodes[i].trim();
			}

			var path = basePath + nodes.join('/');
			if (path.endsWith(".webm") === false ) {
				return '';
			}

			return path;
		},

		/**
		 * Bind the mode change event. [Current user or all user folders ? ]
		 */
		bindUserMode: function() {
			$('#' + _modeTriggerSelector).change(function(){
				var url = allUserVideos;

				if ($(this).is(":checked") == true) {
					url = currentUserVideos;
				}

				window.location.href = url;
			});
		},

		/**
		 * Returns TRUE if tree block exists on the current page
		 * @returns {boolean}
		 */
		isFound: function() {
			return document.getElementById(_selector) !== null;
		}

	});
});