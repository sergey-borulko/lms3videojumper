modules.define('preview', [], function(provide) {

	var _self,
		_videoID = 'preview',
		_instance = null;

	provide({

		init: function () {
			_self = this;

			if (this.isVideoExist() === false) {
				return;
			}
			_instance = videojs(_videoID);

		},

		/**
		 * Sets new video as preview
		 * @param url {string}
		 */
		updateVideo: function(url) {
			_instance.src({"type":"video/webm", "src": url});
		},

		/**
		 * Returns TRUE if video exists on the current page
		 * @returns {boolean}
		 */
		isVideoExist: function() {
			return document.getElementById(_videoID) !== null;
		}

	});
});