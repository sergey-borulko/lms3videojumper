modules.define('storageAjax', function(provide) {

	provide({

		/**
		 * Sends an AJAX request that handles Module->removeVideoAction()
		 *
		 * @param path {string}
		 */
		sendRemoveRequest: function (path) {
			$.ajax({
				url: removeUrl,
				type: "GET",
				contentType: "application/json; charset=utf-8",
				cache: false,
				data: { path: path }
			});
		},

		/**
		 * Sends an AJAX request that handles Module->renameVideoAction()
		 *
		 * @param path {string}
		 * @param newName {string}
		 */
		sendRenameRequest: function (path, newName) {
			$.ajax({
				url: renameUrl,
				type: "GET",
				contentType: "application/json; charset=utf-8",
				cache: false,
				data: { path: path, name: newName }
			});
		}

	});
});