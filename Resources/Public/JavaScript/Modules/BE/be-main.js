document.addEventListener("DOMContentLoaded", function(event) {
	modules.require( ['preview', 'videoTree', 'recorder'], function(Preview, VideoTree, Recorder) {
		Preview.init();
		VideoTree.init();
		Recorder.init();
	});
});