modules.define('browser', [], function(provide) {

    provide({

        /**
         * Returns TRUE if user browser is Google Chrome
         */
        isChrome: function () {
            return /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
        }

    });
});