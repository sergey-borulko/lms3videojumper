modules.define('recordUploader', [], function(provide) {

	var _self,
		_selector = 'fileupload',
		_instance = null;

	provide({

		init: function () {
			_self = this;

			if (this.hasExist() === false) {
				return;
			}

			_instance = $('#' + _selector).fileupload({
				done: function () {
					if (typeof recordFinishedRedirect !== "undefined") {
						window.location.href = recordFinishedRedirect;
					}
				},
                progressall: function(e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);

                    var progressBar = $("#uploadProgress");

                    $(progressBar).text(progress + '%');
                    $(progressBar).css('width', progress + '%');
                }
			});
		},

		/**
		 * Adds the file to the input and makes the post request by parent form action
		 * @param file
		 */
		upload: function(file) {
			_instance.fileupload('add', {files: file});
		},

		/**
		 * Returns TRUE if recorder exists on the current page
		 * @returns {boolean}
		 */
		hasExist: function() {
			return document.getElementById(_selector) !== null;
		}

	});
});