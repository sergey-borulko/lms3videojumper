document.addEventListener("DOMContentLoaded", function(event) {
	modules.require( ['video'], function(Video) {
		Video.init();
	});
});