modules.define('video', ['redirector'], function(provide, Redirector) {

	var _self,
		_videoID = 'video-js',
		_currentTime = 0;

	provide({

		init: function () {
			_self = this;

			if (this.isVideoExist() === false) {
				return;
			}

			$(document.getElementsByClassName(_videoID)).each(function( index ) {
				var isAudio = false;
				if ($(this).hasClass('audio')) {
                    isAudio = true;
				}

                var video = videojs(this);
                if (isAudio === true ) {
                    var audio = $('.audio');

                    $(audio).parent().css('padding-bottom', 0).css('height', '60px').css('max-height', '60px');

                    $(audio).find('.vjs-fullscreen-control').remove();

                    $(audio).find('.vjs-big-play-button').click(function() {
                        $(audio).height(30);
                        (audio).parent().css('height', '30px').css('max-height', '30px');
                    });


                }

				_self.bindRedirectEvent(this);

				if ($(this).attr('data-skipping') === "0") {
					_self.disallowForward(video);
				}

			});
		},

		/**
		 * Disables forward actions, but backward actions keeps possible
		 */
		disallowForward: function(videoInstance) {
			videoInstance.on("seeking", function(event) {
				if (_currentTime < videoInstance.currentTime()) {
					videoInstance.currentTime(_currentTime);
				}
			});

			videoInstance.on("seeked", function(event) {
				if (_currentTime < videoInstance.currentTime()) {
					videoInstance.currentTime(_currentTime);
				}
			});

			setInterval(function() {
				if (!videoInstance.paused()) {
					_currentTime = videoInstance.currentTime();
				}
			}, 1000);
		},

		/**
		 * Binds 'redirection to certain page' when video is finished
		 */
		bindRedirectEvent: function(videoInstance) {
			var url = $(videoInstance).attr('data-finish');
			if(url === false || url === undefined ) {
				return;
			}

			videojs(videoInstance).on("ended", function() {
				Redirector.redirectTo(url);
			});
		},

		/**
		 * Returns TRUE if video exists on the current page
		 * @returns {boolean}
		 */
		isVideoExist: function() {
			return document.getElementsByClassName(_videoID) !== null;
		}

	});
});