modules.define('redirector', [], function(provide) {
	provide({

		/**
		 * Redirects from current page to passed url
		 * @param url {string}
		 */
		redirectTo: function (url) {
			window.location.href = url;
		}

	});
});