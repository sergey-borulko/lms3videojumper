<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Lms3.' . $_EXTKEY,
	'Videojumper',
	array(
		'VideoJumper' => 'show',

	),
	// non-cacheable actions
	array(
		'VideoJumper' => '',
		
	)
);
